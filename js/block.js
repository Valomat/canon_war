function Block(x, y, size) {
   this.engine = new BlockEngine(x, y, size);
   this.interface = new BlockInterface(this.engine);


   this.observable = new Observable();
   this.life = new Life(this, 1);

   this.collision = function(others) {}
}

function BlockEngine(x, y, size) {
   this.x = x;
   this.y = y;
   this.size = size;

   this.body = new Body(this);

   this.setPosition = function(x, y) {
      this.body.setPosition(x,y);
   }

   this.setSpeed = function(speedVector) {
      this.body.setSpeed(speedVector);
   }

   this.addForce = function(forceVector) {
      this.body.addForce(forceVector);
   }

   this.move = function(DELTA_T) {
      this.body.move(DELTA_T);
   }
}

function BlockInterface(blockEngine) {
   this.engine = blockEngine;
   this.color = new Color(parseInt(Math.random() * 25) + 175, parseInt(Math.random() * 25) + 125, 0);

   this.draw = function(ctx) {

      ctx.beginPath();
      ctx.fillStyle = this.color.getString();
      ctx.fillRect(this.engine.x, this.engine.y, this.engine.size, this.engine.size);
      ctx.closePath();

   }
}