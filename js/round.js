function Round(w, h, player1, player2, DELTA_T) {
   var DELTA_T = DELTA_T;
   this.player1 = player1;
   this.player2 = player2;
   this.observable = new Observable();



   var ctx = null;
   var width = w;
   var height = h;

   var histogram = [4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 8, 9, 10, 11, 12, 11, 10, 9, 8, 7, 7, 6, 6, 6, 5, 5, 5, 4, 4, 4, 4, 4, 4, 4];

   var map = new Map(width, height, histogram);
   var canons = [];

   canons.push(new Canon(100, 30, width/50, width, height));
   canons.push(new Canon(width - 100, 30, width/50, width, height));

   map.engine.addObject(canons[0]);
   map.engine.addObject(canons[1]);

   /*
      Initialize players for this round
   */
   player1.setCanon(canons[0]);
   player1.setMap(map);
   player1.observable.registerObserver(this);
   player2.setCanon(canons[1]);
   player2.setMap(map);
   player2.observable.registerObserver(this);

   /*
      Set canvas context
   */
   this.setCanvasCTX = function(_ctx) {
      ctx = _ctx;
   }

   this.update = function(msg, entity) {
      if (player1 == entity) {
         player2.score.addScore(1);
      } else {
         player1.score.addScore(1);
      }

      player1.observable.removeObserver(this);
      player2.observable.removeObserver(this);
      this.observable.notifyObservers("END OF ROUND", entity);
   }

   /*
      set Game area width
   */
   this.setWidth = function(w) {
      width = w;
   }

   /*
      Set game area height
   */
   this.setHeight = function(h) {
      height = h;
   }


   this.core = function(BEAT) {

      if (BEAT % 50 === 0) {
      }

      /*
         Let the players play
      */
      this.player1.play();
      this.player2.play();

      /*
         Apply map physics and display
      */
      map.engine.core(DELTA_T);
      map.interface.draw(ctx);
   }
}