function Body(engine, w, h) {
   var engine = engine;

   this.force = [0, 0];
   this.speed = [0, 0];

   var width = w;
   var height = h;

   this.observable = new Observable;

   this.setPosition = function(x, y) {
      engine.x = x;
      engine.y = y;
   }

   this.setSpeed = function(speedVector) {
      this.speed[0] = speedVector[0];
      this.speed[1] = speedVector[1];
   }

   this.addForce = function(forceVector) {
      this.force[0] += forceVector[0];
      this.force[1] += forceVector[1];
   }

   this.move = function(DELTA_T) {
      this.speed[0] += this.force[0];
      this.speed[1] += this.force[1];

      engine.x += this.speed[0] * DELTA_T;
      engine.y += this.speed[1] * DELTA_T;

      this.force[0] = 0;
      this.force[1] = 0;

      if (engine.x + engine.size < 0 
         || engine.x + engine.size > width 
         || engine.y + engine.size > height) {
         this.observable.notifyObservers("OUT", this);

      }
   }

}