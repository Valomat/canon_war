function Animation(images, stepFunction, duration, deltaT ) {
	var DELTA_T = deltaT;

	this.duration = duration;
	this.images = images;
	this.stepFunction = stepFunction;
	this.duration = duration;

	var currentTime = 0;
	var currentFrame = 0;
	var lol = 0;

	this.finished = function() {
		return currentTime + currentFrame * (this.duration / this.images.length) >= duration;
	}

	this.anim = function(ctx) {
		if ( this.finished() )
			return;
		this.stepFunction(ctx, this.images[currentFrame], currentTime);



		if (currentTime > this.duration / this.images.length) {
			currentTime = currentTime - this.duration / this.images.length;
			currentFrame++;
		}
		lol++;
		currentTime += DELTA_T;
	}

}