var ARROW_LEFT = 37,
	 ARROW_UP = 38,
	 ARROW_RIGHT = 39,
	 ARROW_DOWN = 40,
	 W = 87,
	 A = 65,
	 S = 83, 
	 D = 68;




function Game(width, height) {

	var _this = this;

	var DELTA_T = 1/50;
	var BEAT = 0;

	var heart;

	var score = 0;
	var $scorePanel = null;

	var c = document.getElementById("canvas");
	var ctx = c.getContext("2d");

	var width = $("#canvas").width();
	var height = $("#canvas").height();

	var keyController = new KeyController([ARROW_LEFT, ARROW_UP, ARROW_RIGHT, ARROW_DOWN, W, A, S, D]);


	var player1 = new Player(keyController);
	player1.SHOOTKEY = W;
	player1.LEFTKEY = A;
	player1.RIGHTKEY = D;

	var player2 = new Player(keyController);
	player2.SHOOTKEY = ARROW_UP;
	player2.LEFTKEY = ARROW_LEFT;
	player2.RIGHTKEY = ARROW_RIGHT;

	var round = new Round(width, height, player1, player2, DELTA_T);
	round.setCanvasCTX(ctx);
	round.observable.registerObserver(this);

	this.update = function(msg, entity) {
		if (msg == "END OF ROUND") {
			clearInterval(heart);

			if (player1.score.current == 2) {
				displayScores("Player 1");
				return;
			} else if (player2.score.current == 2) {
				displayScores("Player 2");
				return;
			}

			setTimeout(this.reset, 1000);
		}
	}

	this.reset = function() {
		round = new Round(width, height, player1, player2, DELTA_T);
		round.setCanvasCTX(ctx);
		round.observable.registerObserver(_this);
		_this.start();
	}

	function gameRound() {
		round.core(BEAT);



      if (BEAT % 10 === 0)
         ui();

      BEAT = (BEAT + 1) % (1 / DELTA_T);
	}

	function ui() {

		player1.score.displayScore(scorePanel);
		player2.score.displayScore(scorePanel);
	}

	this.setScorePanel = function(container) {
		scorePanel = container;
	}


	this.start = function() {
		$(scorePanel).removeClass("hidden");

		heart = setInterval(function() {
			gameRound();
		}, DELTA_T * 1000);
	}
	this.pause = function() {
		console.log("pause")
		clearInterval(heart);
	}

	function displayScores(name) {
		$("#score").show();
		$("#score .score").html(name + " has won the game !");
	}
}

