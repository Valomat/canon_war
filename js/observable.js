function Observable() {
   this.observers = [];

   this.registerObserver = function(observer) {
      this.observers.push(observer);
   }

   this.notifyObservers = function(msg, entity) {
      for (var i = 0; i < this.observers.length; ++i) {
         this.observers[i].update(msg, entity);
      }
   }

   this.removeObserver = function(observer) {
      var i = 0; 
      while (this.observers[i] != observer) {
         ++i;
      }

      this.observers.splice(i, 1);
   }
}