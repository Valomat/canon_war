function Bullet(x, y, sx, sy, size, w, h) {
	this.engine = new BulletEngine(x, y, sx, sy, size, w, h);
   this.interface = new BulletInterface(this.engine);

   this.engine.observable.registerObserver(this);
   this.update = function(msg, entity) {
      this.life.oneShot();
   }

   this.observable = new Observable();
   this.life = new Life(this, 100);

   this.collision = function(others) {
      for (var i = 0; i < others.length; ++i) {
         others[i].life.decrease(this.life.max);
      }
      this.life.decrease(this.life.max);
   }
}
Bullet.count = 0;

function BulletEngine(x, y, sx, sy, size, w, h) {
   this.x = x;
   this.y = y;
   this.size = size;

   this.observable = new Observable();

   this.body = new Body(this, w, h);
   this.body.setSpeed([sx, sy]);
   this.body.observable.registerObserver(this);

   this.update = function(msg, entity) {
      this.observable.notifyObservers(msg, entity);
   }

   this.setPosition = function(x, y) {
      this.body.setPosition(x,y);
   }

   this.setSpeed = function(speedVector) {
      this.body.setSpeed(speedVector);
   }

   this.addForce = function(forceVector) {
      this.body.addForce(forceVector);
   }

   this.move = function(DELTA_T) {
      this.body.move(DELTA_T);
   }
}

function BulletInterface(bulletEngine) {
   this.engine = bulletEngine;


   this.draw = function(ctx) {
      ctx.beginPath();
      ctx.arc(this.engine.x + this.engine.size/2, this.engine.y + this.engine.size/2, this.engine.size/2, 0, 2 * Math.PI, false);
      ctx.fillStyle = "black";
      ctx.fill();
      ctx.closePath();
      ctx.drawImage(
         BulletInterface.img,
         this.engine.x, 
         this.engine.y,
         this.engine.size,
         this.engine.size);
   }
}
BulletInterface.img = new Image();
BulletInterface.img.src = "img/bullet.png";