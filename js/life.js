function Life(entity, max) {
   this.entity = entity;
	this.max = max;
   this.current = max;

   this.decrease = function(amount) {
      this.current -= amount;
      this.checkNotify();
   }

   this.oneShot = function() {
      this.current = 0;
      this.checkNotify();
   }

   this.checkNotify = function() {
      if (this.current <= 0) {
         this.notifyDeath();
      }
   }

   this.notifyDeath = function() {
      this.entity.observable.notifyObservers("DEATH", entity);
   }
}