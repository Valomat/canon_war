function Color(r, g, b) {
   this.r = r;
   this.g = g;
   this.b = b;

   this.getString = function() {
      return "rgb(" + this.r + ", " + this.g + ", " + this.b + ")";
   }
}