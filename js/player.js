function Player(keyController) {
   this.LEFTKEY;
   this.RIGHTKEY;
   this.SHOOTKEY;

   var recoveryTime = 40;
   var recovering = recoveryTime;

   var map = null;


   this.ID = Player.count++;
   this.keyControl = keyController;

   this.canon;

   this.score = new Score(2);

   this.observable = new Observable();

   this.setMap = function(m) {
      map = m;
   }

   this.setCanon = function(canon) {
      this.canon = canon;

      this.canon.engine.observable.registerObserver(this);
   }

   this.update = function(msg, entity) {
      this.observable.notifyObservers("LOST", this);
   }

   this.play = function() {

      if ( keyController.isPressed(this.SHOOTKEY) ) {

         if (recovering >= recoveryTime) {
            map.engine.addObject( this.canon.engine.fire() );
            recovering = 0;
         }
      }
      ++recovering;

      if ( keyController.isPressed(this.LEFTKEY) ) {
         this.canon.engine.addAngle(0.03);
      }
      if ( keyController.isPressed(this.RIGHTKEY) ) {
         this.canon.engine.addAngle(-0.03);
      }
   }

}
Player.count = 0;