/*
   Map encapsulation for both MapEngine and MapInterface.
*/
function Map(width, height, histogram) {
   this.engine = new MapEngine(width, height, histogram);
   this.interface = new MapInterface(this.engine);
}

/*
   Map behaviour
   param:
      width: width in pixels
      height: height in pixels
      histogram: array of values giving the number of Blocks per column
*/
function MapEngine(width, height, histogram) {

   // Attributes
   this.width = width;
   this.height = height;
   this.blockSize = this.width / histogram.length;   
   this.mapObjects = [];

   var gravity = [0, width / 110];
   _gravity = [0, -width / 110];


   // Construct the map 
   this.blocks = new Array(histogram.length);
   for (var i = 0; i < histogram.length; ++i) {

      this.blocks[i] = new Array(histogram[i]);

      for (var j = 0; j < histogram[i]; ++j) {
         this.blocks[i][j] = new Block(i * this.blockSize, this.height - j * this.blockSize - this.blockSize, this.blockSize);
         if (j === histogram[i] - 1)
            this.blocks[i][j].interface.color = new Color(127, parseInt(Math.random() * 55) + 200, 127);
         this.blocks[i][j].observable.registerObserver(this);
      }
   }

   /*
      Adds an object to the map. 
      param:
         o: the object to add, must have a setPosition(x, y) method
         x: the x position to which set the object
   */
   this.addObject = function(o) {
      this.mapObjects.push(o);
      o.observable.registerObserver(this);
   }

   this.update = function(msg, entity) {
      if (msg === "DEATH") {
         for (var i = this.mapObjects.length - 1; i >= 0; --i) {
            if (this.mapObjects[i] == entity) {
               this.mapObjects.splice(i, 1);
               return;
            }
         }

         for (var i = this.blocks.length - 1; i >= 0; --i) {
            for (var j = this.blocks[i].length - 1; j >= 0; --j) {
               if (this.blocks[i][j] == entity) {
                  this.blocks[i].splice(j, 1);
                  if (j > 0)
                     this.blocks[i][j - 1].interface.color = new Color(127, parseInt(Math.random() * 55) + 200, 127);
               }
            }
         }
      }
   }

   this.core = function(DELTA_T) {

      for (var i = 0; i < this.mapObjects.length; ++i) {

         var o = this.mapObjects[i];
         if (o == undefined)
            return;

         // Apply gravity
         o.engine.addForce(gravity);


         /*
            Move object
         */
         o.engine.move(DELTA_T);

         /*
            Get interesting values
         */
         // Size of object
         var size = o.engine.size;

         // left corner x position
         var leftX = o.engine.x;
         // right corner x position
         var rightX = o.engine.x + o.engine.size;

         // Bottom y position
         var y = o.engine.y + o.engine.size;

         // range of indexes to test
         var index1 = parseInt(leftX / this.blockSize);
         var index2 = parseInt(rightX / this.blockSize);

         if (index1 >= 0 && index2 < this.blocks.length) {

            // Collide state and min y position at which collision happens
            var collide = false;
            var minY = this.height;
            var collisionBlocks = [];

            // Check all Block inside the range of indexes
            for (var j = index1; j <= index2; ++j) {
               if (this.blocks[j].length > 0) {               
                  var boundY = this.blocks[j][this.blocks[j].length - 1].engine.y;
                  collide = collide || y > boundY;

                  if (y > boundY)
                     collisionBlocks.push(this.blocks[j][this.blocks[j].length - 1]);

                  if (boundY < minY) {
                     minY = boundY;

                  }
               }
            }

            // Cancel gravity if collisions happen
            if ( collide ) {
               this.mapObjects[i].engine.addForce(_gravity);
               this.mapObjects[i].engine.setSpeed([0, 0]);
               this.mapObjects[i].engine.setPosition(leftX, minY - size + 1);

               for (var j = 0; j < collisionBlocks.length; ++j) {
                  collisionBlocks[j].collision(o);
               }
               o.collision(collisionBlocks);
            }
         }
      }
   }
}


/*
   Map interface
   Manages the interface of the map.
*/
function MapInterface(engine) {
   this.engine = engine;

   this.draw = function(ctx) {
      /*
         Reset canvas
      */
      ctx.clearRect(0, 0, this.engine.width, this.engine.height);

      ctx.fillStyle = "rgb(150, 225, 255";
      ctx.fillRect(0, 0, this.engine.width, this.engine.height);

      /*
         Draw Blocks
      */
      for (var i = 0; i < this.engine.blocks.length; ++i) {
         for (var j = 0; j < this.engine.blocks[i].length; ++j) {
            this.engine.blocks[i][j].interface.draw(ctx);
         }
      } 

      /*
         Draw map objects
      */ 
      for (var i = 0; i < this.engine.mapObjects.length; ++i) {
         this.engine.mapObjects[i].interface.draw(ctx);
      } 
   }

   this.engine.mapObjects
}