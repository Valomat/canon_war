function KeyController(keyCodes) {

   var keys = [];

   for (var i = 0; i < keyCodes.length; ++i) {
      keys.push({"keyCode": keyCodes[i], "state": false});
   }

   document.onkeydown = function (e) {
      e = e || window.event;
      
      for (var i = 0; i < keys.length; ++i) {
         if (keys[i].keyCode === e.which) {
            keys[i].state = true;
         }
      }
   };

   document.onkeyup = function (e) {
      e = e || window.event;
      
      for (var i = 0; i < keys.length; ++i) {
         if (keys[i].keyCode == e.which) {
            keys[i].state = false;
         }
      }
   };

   this.isPressed = function(keyCode) {
      for (var i = 0; i < keys.length; ++i) {
         if (keys[i].keyCode === keyCode) {
            return keys[i].state;
         }
      }
      return false;
   }
}