function Score(max) {
   this.max = max;
   this.current = 0;
   this.id = ++Score.count;

   this.addScore = function(amount) {
      this.current += amount;
   }

   this.displayScore = function(container) {
      for (var i = 0; i < this.current; ++i) {
         $(container + " .score-player" + this.id + " .div" + (i + 1) ).addClass("on");
      }
   }
}

Score.count = 0;