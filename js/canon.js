function Canon(x, y, size, w, h) {
   this.engine = new canonEngine(x, y, size, w, h);
   this.interface = new canonInterface(this.engine);

   this.life = new Life(this, 500);
   this.observable = new Observable();

   this.collision = function(others) {}
}


function canonEngine(x, y, size, w, h) {
   this.x = x;
   this.y = y;
   this.size = size;
   this.angle = Math.PI/2;

   this.body = new Body(this, w, h);
   this.body.observable.registerObserver(this);

   var BULLET_SPEED = size * 30;
   var BULLET_SIZE = size * 1.2;

   this.observable = new Observable(this);

   this.update = function(msg, entity) {
      this.observable.notifyObservers(msg, entity);
   }

   this.setPosition = function(x, y) {
      this.body.setPosition(x,y);
   }

   this.setSpeed = function(speedVector) {
      this.body.setSpeed(speedVector);
   }

   this.addForce = function(forceVector) {
      this.body.addForce(forceVector);
   }

   this.move = function(DELTA_T) {
      this.body.move(DELTA_T);
   }

   this.addAngle = function(angle) {
      var newAngle = this.angle + angle;
      if (newAngle >= 0 && newAngle <= Math.PI)
         this.angle = newAngle;
   }

   this.fire = function() {
      var x = this.x - BULLET_SIZE/2 + Math.cos(this.angle) * this.size * 4;
      var y = this.y - BULLET_SIZE/2 - Math.sin(this.angle) * this.size * 4;

      var speedX = Math.cos(this.angle) * BULLET_SPEED;
      var speedY = -Math.sin(this.angle) * BULLET_SPEED;

      return new Bullet(x, y, speedX, speedY, BULLET_SIZE, w, h);
   }
}

function canonInterface(canonEngine) {
   this.engine = canonEngine;

   this.draw = function(ctx) {
      ctx.fillStyle = 'black';
      ctx.translate(this.engine.x + this.engine.size / 2, this.engine.y + this.engine.size / 2);
      ctx.rotate(-this.engine.angle);
      ctx.drawImage(
         canonInterface.img,
         - this.engine.size / 2, 
         - this.engine.size / 2, 
         this.engine.size * 4, 
         this.engine.size);
      ctx.rotate(this.engine.angle);
      ctx.translate(-this.engine.x - this.engine.size / 2,-this.engine.y - this.engine.size / 2);

      ctx.beginPath();
      ctx.fillStyle = 'grey';
      ctx.arc(this.engine.x + this.engine.size/2, this.engine.y + this.engine.size, this.engine.size, Math.PI, 0, false);
      ctx.fill();
      ctx.closePath();
   }  
}
canonInterface.img = new Image();
canonInterface.img.src = "img/canon.png";