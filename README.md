# Canon War #
# Two players HTML5 / Javascript game #

Canon War is a two players game, in which your goal is to destroy the map below the enemy canon. The first player to make the opponent fall wins the round. Best of three.

## Controls ##

### Player 1
**Left:** A

**Right:** D

**Shoot:** W

### Player 2 ###
**Left:** LEFT ARROW

**Right:** RIGHT ARROW

**Shoot:** UP ARROW